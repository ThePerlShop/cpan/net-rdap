#!/usr/bin/env perl

use strict;
use warnings;

package Test::Net::RDAP;

use Test2::V0;
use Test2::Tools::Spec;

use Data::Dumper;
use Path::Tiny qw(tempdir);

use Net::DNS::Domain;

# load code to be tested
use Test2::Tools::Target 'Net::RDAP';


=head1 NAME

Test::Net::RDAP - Test L<Net::RDAP>

=head1 SYNOPSIS

    # run all tests
    prove -lv t/domain.t

    # run single test method
    T2_WORKFLOW=test_METHOD_NAME prove -lv t/domain.t

=cut


=head1 TESTS

=head2 test_domain

Retrieve a domain record from the appropriate RDAP server

=cut

tests test_domain => sub {
    plan 1;

	my $name   = 'theperlshop.com';
	my $domain = Net::DNS::Domain->new( $name );
	my $rdap   = $CLASS->new();
	my $result = $rdap->domain( $domain );

	is(
		$result,
		object {
			prop isa => 'Net::RDAP::Object::Domain';

			call class       => 'domain';

			call handle      => match( qr/\d+_DOMAIN_COM-VRSN/x );

			call port43      => undef;

			call_list conformance => [
				'rdap_level_0',
				'icann_rdap_technical_implementation_guide_0',
				'icann_rdap_response_profile_0',
			];

			call_list notices     => array {
				all_items object { prop isa => 'Net::RDAP::Notice' }
			};

			call_list status => [ 'client transfer prohibited' ];

			call_list remarks     => array {
				all_items object { prop isa => 'Net::RDAP::Object::Remark' }
			};

			call_list events => array {
				all_items object { prop isa => 'Net::RDAP::Event' }
			};

			call_list ids => array {
				all_items object { prop isa => 'Net::RDAP::ID' }
			};

			call_list entities => array {
				all_items object { prop isa => 'Net::RDAP::Object::Entity' }
			};

			call name => object {
				prop isa => 'Net::DNS::Domain';
			};

			call_list keys => [];

			call_list nameservers => array {
				all_items object { prop isa => 'Net::RDAP::Object::Nameserver' }
			};

			call_list ds => array {
				all_items object { prop isa => 'Net::DNS::RR::DS' }
			};

			call zoneSigned => F();

			call delegationSigned => F();

			call maxSigLife => undef;

			call network => object {
				prop isa => 'Net::RDAP::Object::IPNetwork';
			};

			end;
		},
		'domain object contains the expected data'
	);
};


done_testing();
